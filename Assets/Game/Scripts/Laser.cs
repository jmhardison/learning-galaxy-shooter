﻿using UnityEngine;

namespace Game.Scripts
{
    public class Laser : MonoBehaviour
    {

        [SerializeField]
        private float _speed = 10.0f;


        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            transform.Translate(Vector3.up * _speed * Time.deltaTime);

            if (transform.position.y > 6.0f)
            {
                //destroy the object as it is off the screen
                if (transform.parent != null)
                {
                    Destroy(transform.parent.gameObject);
                }
                Destroy(this.gameObject);
            }

        }


    }
}
