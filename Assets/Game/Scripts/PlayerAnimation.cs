﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Game.Scripts
{
    public class PlayerAnimation : MonoBehaviour
    {
        [SerializeField] private Animator _playerAnimator = null;

        private int _playerNumber = 0;

        // Start is called before the first frame update
        void Start()
        {
            _playerAnimator = this.GetComponent<Animator>();
            _playerNumber = this.GetComponent<Player>().PlayerNumber;
        }

        // Update is called once per frame
        void Update()
        {
            //TODO:Need to refactor for co-op?
            if (_playerNumber == 1)
            {

                if (Input.GetKeyDown(KeyCode.A))
                {
                    _playerAnimator.SetBool("TurnLeft", true);
                    _playerAnimator.SetBool("TurnRight", false);
                }

                if (Input.GetKeyUp(KeyCode.A))
                {
                    _playerAnimator.SetBool("TurnLeft", false);
                }

                if (Input.GetKeyDown(KeyCode.D))
                {
                    _playerAnimator.SetBool("TurnLeft", false);
                    _playerAnimator.SetBool("TurnRight", true);
                }

                if (Input.GetKeyUp(KeyCode.D))
                {
                    _playerAnimator.SetBool("TurnRight", false);
                }
            }
            
            if (_playerNumber == 2)
            {

                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    _playerAnimator.SetBool("TurnLeft", true);
                    _playerAnimator.SetBool("TurnRight", false);
                }

                if (Input.GetKeyUp(KeyCode.LeftArrow))
                {
                    _playerAnimator.SetBool("TurnLeft", false);
                }

                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    _playerAnimator.SetBool("TurnLeft", false);
                    _playerAnimator.SetBool("TurnRight", true);
                }

                if (Input.GetKeyUp(KeyCode.RightArrow))
                {
                    _playerAnimator.SetBool("TurnRight", false);
                }
            }
        }
    }
}
