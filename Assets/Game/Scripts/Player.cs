﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


namespace Game.Scripts
{
    public class Player : MonoBehaviour
    {
        [SerializeField] public int PlayerNumber;
        
        [SerializeField]
        private float _speed = 5.0f;

        [SerializeField]
        private GameObject _laserPrefab = null;

        [SerializeField]
        private GameObject _trippleShotPrefab = null;

        [SerializeField]
        private GameObject _deathAnimation = null;

        [SerializeField]
        private GameObject _hurtAnimationLeft = null;

        [SerializeField]
        private GameObject _hurtAnimationRight = null;

        [SerializeField]
        private GameObject _shield = null;

        [SerializeField]
        private int _lives = 3;

        [SerializeField]
        private AudioClip _laserAudioClip = null;

        [SerializeField]
        private AudioClip _powerupAudioClip = null;

        private AudioSource _audioSource = null;

        //fire rate
        [SerializeField]
        private float _fireRate = 0.25f;
        //can fire variable
        private float _nextFire = 0.0f;
        //time.time how much time.

        //powerups
        private bool _hasTrippleShot = false;
        private bool _hasSpeedBoost = false;
        private bool _hasShield = false;

        private UIManager _uiManager = null;
        private GameManager _gameManager = null;

        // Start is called before the first frame update
        void Start()
        {
            _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
            _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            _uiManager.UpdateLives(_lives, this.name);
            _uiManager.ResetScore();
            _audioSource = this.GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            Movement();
#if UNITY_ANDROID
            //if player 1, space shoots.
            if (((CrossPlatformInputManager.GetButtonDown("Fire")) && PlayerNumber == 1) && Time.time > _nextFire)
            {
                Shoot();
                
            }            
#elif UNITY_IOS
            //if player 1, space shoots.
            if (((CrossPlatformInputManager.GetButtonDown("Fire")) && PlayerNumber == 1) && Time.time > _nextFire)
            {
                Shoot();
                
            } 

#else //any other platform
            
            //if player 1, space shoots.
            if (((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))& PlayerNumber == 1) && Time.time > _nextFire)
            {
                Shoot();
                
            }
            
            //if player 2, right control shoots
            if ((Input.GetKeyDown(KeyCode.RightControl) && PlayerNumber == 2) && Time.time > _nextFire)
            {
                Shoot();
                
            }
#endif


            
        }

        private void Shoot()
        {
            if (!(Time.timeScale >= 1)) return;
            if (_hasTrippleShot)
            {
                Instantiate(_trippleShotPrefab, transform.position, Quaternion.identity);

            }
            else
            {
                //instantiate new laser at current position
                Instantiate(_laserPrefab, transform.position + new Vector3(0, 0.93f, 0), Quaternion.identity);
            }

            _audioSource.PlayOneShot(_laserAudioClip);
            _nextFire = Time.time + _fireRate;
        }

        private void Movement()
        {
            float horizontalInput = 0.0f;
            float verticalInput = 0.0f;
            
            if (PlayerNumber == 1)
            {
                horizontalInput = CrossPlatformInputManager.GetAxis("Horizontal");
                verticalInput = CrossPlatformInputManager.GetAxis("Vertical");
            }
            else if (PlayerNumber == 2)
            {
                horizontalInput = Input.GetAxis("P2_Horizontal");
                verticalInput = Input.GetAxis("P2_Vertical");
            }

            if (_hasSpeedBoost)
            {
                transform.Translate(Vector3.right * (_speed * 2) * horizontalInput * Time.deltaTime);
                transform.Translate(Vector3.up * (_speed * 2) * verticalInput * Time.deltaTime);
            }
            else
            {
                transform.Translate(Vector3.right * _speed * horizontalInput * Time.deltaTime);
                transform.Translate(Vector3.up * _speed * verticalInput * Time.deltaTime);
            }

            if (transform.position.y > 0)
            {
                transform.position = new Vector3(transform.position.x, 0, 0);
            }
            else if (transform.position.y < -4.2f)
            {
                transform.position = new Vector3(transform.position.x, -4.2f, 0);
            }

            //-9.8 on leftx
            if (transform.position.x < -9.8f)
            {
                transform.position = new Vector3(9.8f, transform.position.y, 0);
            }
            //+9.8 on rightx
            else if (transform.position.x > 9.8f)
            {
                transform.position = new Vector3(-9.8f, transform.position.y, 0);
            }


        }

        public void RemoveLife()
        {
            if (!_hasShield)
            {
                if (_lives > 1)
                {
                    _lives--;
                    _uiManager.UpdateLives(_lives, this.name);
                    if (_lives == 2)
                    {
                        _hurtAnimationRight.SetActive(true);
                    }
                    if (_lives == 1)
                    {
                        _hurtAnimationLeft.SetActive(true);
                    }
                }
                else
                {
                    _lives--;
                    _uiManager.UpdateLives(_lives, this.name);
                    //destroy self because we are out of lives.
                    DestroyFromHit();
                }
            }
            else if (_hasShield)
            {
                ShieldOff();
            }
        }



        public void TrippleShotOn()
        {
            _audioSource.PlayOneShot(_powerupAudioClip);
            _hasTrippleShot = true;
            StartCoroutine(TrippleshotPowerdown());

        }

        public void ShieldOn()
        {
            _audioSource.PlayOneShot(_powerupAudioClip);
            _hasShield = true;
            _shield.SetActive(true);

            //transform.GetChild(0).gameObject.SetActive(true);
        }

        public void ShieldOff()
        {
            _hasShield = false;
            _shield.SetActive(false);
            //transform.GetChild(0).gameObject.SetActive(false);
        }

        IEnumerator TrippleshotPowerdown()
        {
            yield return new WaitForSeconds(5.0f);
            _hasTrippleShot = false;
        }

        public void SpeedOn()
        {
            _audioSource.PlayOneShot(_powerupAudioClip);
            _hasSpeedBoost = true;
            StartCoroutine(SpeedPowerDown());
        }

        IEnumerator SpeedPowerDown()
        {
            yield return new WaitForSeconds(5.0f);
            _hasSpeedBoost = false;
        }

        public void DestroyFromHit()
        {
            Instantiate(_deathAnimation, transform.position, Quaternion.identity);
            _gameManager.KillPlayer(this.transform.name);
            Destroy(this.gameObject);
        }

    }
}
