﻿using UnityEngine;

namespace Game.Scripts
{
    public class Powerup : MonoBehaviour
    {
        [SerializeField]
        private float _speed = 3.0f;

        [SerializeField]
        private int _powerupID = 0; //0 = trippleshot, 1 = speed, 2 = shield

    

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            transform.Translate(Vector3.down * _speed * Time.deltaTime);

            //if y < lower move to top.
            if (transform.position.y < -6.11f)
            {
                //destroy the object as it's off screen.
                Destroy(this.gameObject);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                Player playerObj = other.GetComponent<Player>();
            

                if (playerObj != null)
                {
                    switch (_powerupID)
                    {
                        case 0:
                            //trippleshot is picked up
                            playerObj.TrippleShotOn();
                            break;
                        case 1:
                            //speed boost is picked up
                            playerObj.SpeedOn();
                            break;
                        case 2:
                            //do thing for shield
                            playerObj.ShieldOn();
                            break;
                        default:
                            //do nothing
                            break;
                    }
                

                }
            
                Destroy(this.gameObject);
            }
        }
    }
}
