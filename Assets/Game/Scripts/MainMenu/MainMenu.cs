﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scripts.MainMenu
{
    public class MainMenu : MonoBehaviour
    {
        //Scene Names //
        //CoOpMode
        //SinglePlayer
        [SerializeField] private GameObject _buttonCoop;
        [SerializeField] private GameObject _buttonQuit;
        
        private void Start()
        {
#if UNITY_ANDROID
            //disable coop button and quit
            _buttonCoop.SetActive(false);
            _buttonQuit.SetActive(false);
#elif UNITY_IOS
            //disable coop button and quit
            _buttonCoop.SetActive(false);
            _buttonQuit.SetActive(false);
#endif
        }

        public void StartSinglePlayer()
        {
            SceneManager.LoadScene("SinglePlayer", LoadSceneMode.Single);
        }

        public void StartCoOpMode()
        {
            #if UNITY_ANDROID
            //do nothing, no coop
            #elif UNITY_IOS
            //do nothing no coop
            #else
            SceneManager.LoadScene("CoOpMode", LoadSceneMode.Single);

            #endif
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
