﻿using UnityEngine;

namespace Game.Scripts
{
    public class DestroyAnimation : MonoBehaviour
    {
        [SerializeField]
        private float _destroyDelay = 0f;

        private AudioSource _explosionAudioSource = null;

        // Start is called before the first frame update
        void Start()
        {
            _explosionAudioSource = this.GetComponent<AudioSource>();
            _explosionAudioSource.Play();
            Destroy(gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + _destroyDelay);
        }
    }
}
