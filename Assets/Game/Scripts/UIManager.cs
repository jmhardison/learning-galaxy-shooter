﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts
{
    public class UIManager : MonoBehaviour
    {
        
        [SerializeField]
        private Sprite[] _livesimages = null;

        [SerializeField]
        private Image _livesUI = null;

        [SerializeField] private Image _livesUI2 = null;
        
        [SerializeField]
        private TextMeshProUGUI _scoreText = null;

        [SerializeField] private TextMeshProUGUI _highScoreText = null;
        
        [SerializeField]
        private Image _titleImage = null;

        private int _currentScore = 0;
        private int _highScore = 0;
        public void UpdateLives(int currentLives, string transformName)
        {
            if (transformName == "Player(Clone)" || transformName == "Player1(Clone)")
            {
                _livesUI.sprite = _livesimages[currentLives];
            }
            else
            {
                _livesUI2.sprite = _livesimages[currentLives];
            }
        }

        public void UpdateScore(int addScore)
        {
            _currentScore += addScore;
            _scoreText.text = "Score: " + _currentScore;
            if (_highScoreText != null)
            {
                checkBestScore();
            }
        }

        public void ResetScore()
        {
            _currentScore = 0;
            _scoreText.text = "Score: " + _currentScore;
        }

        public void ShowGameUI()
        {
            _scoreText.gameObject.SetActive(true);
            _livesUI.gameObject.SetActive(true);
            _titleImage.gameObject.SetActive(false);
            if (_livesUI2 != null) _livesUI2.gameObject.SetActive(true);
            if (_highScoreText != null) _highScoreText.gameObject.SetActive(true);
        }

        public void ShowTitleUI()
        {
            _scoreText.gameObject.SetActive(false);
            _livesUI.gameObject.SetActive(false);
            _titleImage.gameObject.SetActive(true);
            if (_livesUI2 != null) _livesUI2.gameObject.SetActive(false);
            if (_highScoreText != null)
            {
                LoadHighScore();
                UpdateHighScore();
                _highScoreText.gameObject.SetActive(false);
            }
        }

        private void checkBestScore()
        {
            if (_currentScore > _highScore)
            {
                _highScore = _currentScore;
                SaveHighScore();
                UpdateHighScore();
            }
        }
        private void UpdateHighScore()
        {
            _highScoreText.text = "High Score: " + _highScore;
        }

        private void SaveHighScore()
        {
            PlayerPrefs.SetInt("HighScore", _highScore);
            PlayerPrefs.Save();
        }

        private void LoadHighScore()
        {
            _highScore = PlayerPrefs.GetInt("HighScore", _currentScore);
        }
        



    }
}
