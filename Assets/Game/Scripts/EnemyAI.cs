﻿using UnityEngine;

namespace Game.Scripts
{
    public class EnemyAI : MonoBehaviour
    {
        [SerializeField]
        private float _speed = 2.5f;



        [SerializeField]
        private GameObject _deathAnimation = null;

        private UIManager _uiManager = null;


        // Start is called before the first frame update
        void Start()
        {
            //need a random 'x' position. Each time the enemy is drawn its horizontal is random.
            //transform.position = new Vector3(Random.Range(-8, 8), 3.78f, 0);
            _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        }

        // Update is called once per frame
        void Update()
        {
            // call movement to move down screen
            Movement();
        }


        private void Movement()
        {
            transform.Translate(Vector3.down * _speed * Time.deltaTime);

            //if y < lower move to top.
            if (transform.position.y < -6.11f)
            {
                //move back to top at a random location.
                transform.position = new Vector3(Random.Range(-8, 8), 6.11f, 0);
            }

        }

        /// <summary>
        /// responsible for handling collision of either laser or player
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //check if laser or Player
            //also should have standardized on upper case or something.

            if (collision.tag == "Player")
            {
                //if player, we need to notify the player object that it needs to deduct a life.
                //if Projectile, we need to destroy the laser and self. (future we should add a score)
                Player playerObj = collision.GetComponent<Player>();
                if (playerObj != null)
                {
                    playerObj.RemoveLife();
                    Instantiate(_deathAnimation, transform.position, Quaternion.identity);
                    Destroy(this.gameObject);
                }
            }
            else if (collision.tag == "Projectile")
            {
                Laser laserObj = collision.GetComponent<Laser>();
                if (laserObj != null)
                {
                    if (collision.transform.parent != null)
                    {
                        Destroy(collision.transform.parent.gameObject);
                    }
                    Destroy(collision.gameObject);

                    _uiManager.UpdateScore(10);

                    Instantiate(_deathAnimation, transform.position, Quaternion.identity);
                    Destroy(this.gameObject);
                }
            }
        }


    }
}
