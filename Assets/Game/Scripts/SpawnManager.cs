﻿using System.Collections;
using UnityEngine;

namespace Game.Scripts
{
    public class SpawnManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject _enemyShip = null;

        [SerializeField]
        private GameObject[] _powerUps = null;


        [SerializeField]
        private GameObject _player = null;

        [SerializeField] private GameObject _player2 = null;

        [SerializeField]
        private float _timeForEnemySpawn = 5.0f;

        [SerializeField]
        private float _timeForPowerupSpawn = 10.0f;

        [SerializeField] bool _gameIsStarted = false;

        


        public void StartGame()
        {
            
            _gameIsStarted = true;

            if (_player2 != null)
            {
                //spawn player 1 and 2 adjusting the position to -2 for player1 and +2 for player 2
                Instantiate(_player2, new Vector3(2,0,0), Quaternion.identity);
                Instantiate(_player, new Vector3(-2, 0, 0), Quaternion.identity);
            }
            else
            {
             //spawn player 1 only at zero position
            Instantiate(_player, Vector3.zero, Quaternion.identity);
            }
            
            StartCoroutine(SpawnEnemy());
            StartCoroutine(SpawnPowerup());

        }


        private IEnumerator SpawnEnemy()
        {
            while (_gameIsStarted && Time.timeScale >= 1)
            {
                //spawn enemy at random position. (or let enemy start do it.)
                Instantiate(_enemyShip, new Vector3(Random.Range(-8, 8), 6.11f, 0), Quaternion.identity);

                yield return new WaitForSeconds(_timeForEnemySpawn);
            }
            //cleanup


        }

        private IEnumerator SpawnPowerup()
        {
            while (_gameIsStarted && Time.timeScale >= 1)
            {
                //spawn random powerup at random position.
                Instantiate(_powerUps[Random.Range(0, _powerUps.Length)], new Vector3(Random.Range(-8, 8), 6.11f, 0), Quaternion.identity);

                yield return new WaitForSeconds(_timeForPowerupSpawn);
            }
            //cleanup

        }

        private void CleanupObjects()
        {
            foreach (GameObject child in GameObject.FindGameObjectsWithTag("Protection"))
            {
                Destroy(child);
            }
            foreach (GameObject child in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                Destroy(child);
            }
        }

        public void StopGame()
        {
            //called when player dies.
            _gameIsStarted = false;
            CleanupObjects();
        }
    }
}
