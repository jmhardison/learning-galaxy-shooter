﻿using System.Collections;
using System.Collections.Generic;
using Game.Scripts;
using UnityEngine;

public class PauseGameControl : MonoBehaviour
{
    
    [SerializeField] private GameObject _pausedPanel = null;
    private bool _gameIsPaused = false;
    private GameManager _gameManagerInstance;

    private Animator _pauseAnimator = null;

    private void Start()
    {
        _gameManagerInstance = GameObject.Find("GameManager").GetComponent<GameManager>();
        _pauseAnimator = _pausedPanel.GetComponent<Animator>();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            //should pause game now
            PauseToggle();
        }
    }
    
    
    public void ShowPauseScreen()
    {        
        _pausedPanel.SetActive(true);
        _pauseAnimator.SetBool("isPause", true);
    }

    public void HidePauseScreen()
    {
        _pauseAnimator.SetBool("isPause", false);
        
        _pausedPanel.SetActive(false);
    }
    
    public void PauseToggle()
    {
        if (_gameIsPaused)
        {
            _gameIsPaused = false;
            HidePauseScreen();
            
            //unpause
            Time.timeScale = 1;

        }
        else if (!_gameIsPaused)
        {
            _gameIsPaused = true;
            ShowPauseScreen();
            //pause
            //set timescale to 0
    
            Time.timeScale = 0;
            
        }
    }
}
