﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

namespace Game.Scripts
{
    public class GameManager : MonoBehaviour
    {
        private UIManager _uiManager = null;


        private bool _gameIsStarted = false;
        
        private SpawnManager _spawnManager = null;
        

        public bool isCoOpMode = false;

        public bool isPlayer1Alive = false;
        public bool isPlayer2Alive = false;

        // Start is called before the first frame update
        void Start()
        {
            _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
            _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
            _uiManager.ShowTitleUI();

        }



        // Update is called once per frame
        void Update()
        {
            //check for input if game is not started.
            if (_gameIsStarted) return;

#if UNITY_ANDROID

            StartGame();

#elif UNITY_IOS

                StartGame();
           
#else
if (Input.GetKeyDown(KeyCode.Space))
            {
                StartGame();
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                //go back to main menu
                SceneManager.LoadScene("Menu", LoadSceneMode.Single);
            }
#endif


        }

        private void StartGame()
        {
            //hide UI Elements for Title
            _gameIsStarted = true;
            _uiManager.ShowGameUI();
            _spawnManager.StartGame();

            if (isCoOpMode)
            {
                isPlayer1Alive = true;
                isPlayer2Alive = true;
            }
            else
            {
                isPlayer1Alive = true;
            }

        }

        public void KillPlayer(string transformName)
        {
            if (transformName == "Player(Clone)" || transformName == "Player1(Clone)")
            {
                isPlayer1Alive = false;
            }

            if (transformName == "Player2(Clone)")
            {
                isPlayer2Alive = false;
            }
            
            StopGame();
        }
        
        private void StopGame()
        {

            if (isCoOpMode)
            {
                if (isPlayer1Alive || isPlayer2Alive) return;
            }
            else
            {
                if (isPlayer1Alive) return;
            }

            _spawnManager.StopGame();
            _gameIsStarted = false;
            _uiManager.ShowTitleUI();
        }
    }
}
